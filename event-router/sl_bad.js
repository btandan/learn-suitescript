define(["N/http", "N/error"], function (http, error) {

    /**
     * A bad example of Suitelet handling HTTP requests
     *
     * @exports bad-example/sl
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType Suitelet
     */
    var exports = {};

    /**
     * <code>onRequest</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.request
     *        {ServerRequest} The incoming request object
     * @param context.response
     *        {ServerResponse} The outgoing response object
     *
     * @return {void}
     *
     * @static
     * @function onRequest
     */
    function onRequest(context) {
        switch (context.request.method) {
            case http.Method.GET:
                // Do a search

                // Render a form

                // Write to response

                break;
            case http.Method.POST:
                // Parse form data

                // Process form data

                // Send email

                // Write to response

                break;
            default:
                // Email admin

                throw error.create({
                    name: "SSS_UNSUPPORTED_REQUEST_TYPE",
                    message: "Suitelet only supports GET and POST",
                    notifyOff: true
                });

                break;
        }
    }

    exports.onRequest = onRequest;
    return exports;
});
