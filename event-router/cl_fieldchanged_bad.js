define([], function () {

    /**
     * A bad example of a Client Script handling multiple fieldChanged processes
     *
     * @exports bad-example/cl
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType ClientScript
     */
    var exports = {};

    /**
     * <code>fieldChanged</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.currentRecord
     *        {record} The current record the user is manipulating in the UI
     * @param context.sublistId
     *        {String} The internal ID of the sublist.
     * @param context.fieldId
     *        {String} The internal ID of the field that was changed.
     * @param [context.lineNum=undefined]
     *        {String} The index of the line if the field is in a sublist or
     *            matrix.
     * @param [context.columnNum=undefined]
     *        {String} The index of the column if the field is in a matrix.
     *
     * @return {void}
     *
     * @static
     * @function fieldChanged
     */
    function fieldChanged(context) {
        if (context.fieldId === "entity") {
            // Do something

            // Do something else

        } else if (context.fieldId === "item") {
            // Do the same something

            // Do another thing

            // Do yet another thing
        } else if (context.fieldId === "quantity") {
            // Do a completely different thing

            // Do a somewhat similar thing

            // Do something wrong

            // Do more things

            // Do many more things
        } else if (context.fieldId === "amount") {
            // Do a completely different thing

            // Do a somewhat similar thing

            // Do something wrong
        } else if (context.fieldId === "rate") {
            // Do a completely different thing

            // Do a somewhat similar thing

            // Do something wrong
        }
    }

    exports.fieldChanged = fieldChanged;
    return exports;
});
