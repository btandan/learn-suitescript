define([], function () {

    /**
     * Handles business logic for creation operations for the custom Shipment record
     *
     * @exports xxx/shipment
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     */
    var exports = {};

    /**
     * Creates an instance of the Shipment record (customrecord_xxx_shipment_line
     *
     * @governance 6
     *
     * @param data {Object}
     * @param data.pkgNum {Number} Value to be used for custrecord_xxx_pkgnumber
     *
     * @returns {Number} Internal ID of the Shipment record that was created
     */
    function create(data) {
        log.audit({title: "Translating data to record..."});
        var recObj = rec.create({
            type: 'customrecord_xxx_shipment_line'
        });

        recObj.setValue({
            fieldId: 'custrecord_xxx_pkgnumber',
            value: data.pkgNum
        });
        recObj.setValue({
            fieldId: 'custrecord_xxx_piece_count',
            value: data.pieceCount
        });
        recObj.setValue({
            fieldId: 'custrecord_xxx_packagetype',
            value: data.pkgType
        });
        recObj.setValue({
            fieldId: 'custrecord_xxx_inventoryunits',
            value: data.invUnits
        });
        recObj.setValue({
            fieldId: 'custrecord_xxx_nmfc_number_line',
            value: data.nmfcId
        });
        recObj.setValue({
            fieldId: 'custrecord_xxx_weight',
            value: data.weight
        });
        recObj.setValue({
            fieldId: 'custrecord_xxx_length',
            value: data.length
        });
        recObj.setValue({
            fieldId: 'custrecord_xxx_width',
            value: data.width
        });
        recObj.setValue({
            fieldId: 'custrecord_xxx_height',
            value: data.height
        });
        recObj.setValue({
            fieldId: 'custrecord_xxx_shipment_parent',
            value: data.shipParentId
        });
        var packageId = recObj.save({});
        log.debug({title:"Created Record", details: packageId});

        return packageId;
    }

    exports.create = create;
    return exports;
});
