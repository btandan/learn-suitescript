/**
 * Adds two numbers together
 *
 * @param a {Number} - Left operand
 * @param b {Number} - Right operand
 *
 * @returns {Number} - Sum of operands
 */
function add(a, b) {
    if (!(isNumeric(a) && isNumeric(b))) {
        throw new NonNumericArgumentException(arguments.callee.name);
    }
    // Must additionally parse the arguments, otherwise valid Numbers passed as Strings would be concatenated
    return (parseFloat(a) + parseFloat(b));
}

/**
 * Subtracts two numbers
 *
 * @param a {Number} - Left operand
 * @param b {Number} - Right operand
 *
 * @returns {Number} - Difference of operands
 */
function subtract(a, b) {
    if (!(isNumeric(a) && isNumeric(b))) {
        throw new NonNumericArgumentException(arguments.callee.name);
    }
    return (a - b);
}

/**
 * Multiplies two numbers together
 *
 * @param a {Number} - Left operand
 * @param b {Number} - Right operand
 *
 * @returns {Number} - Product of operands
 */
function multiply(a, b) {
    if (!(isNumeric(a) && isNumeric(b))) {
        throw new NonNumericArgumentException(arguments.callee.name);
    }
    return (a * b);
}

/**
 * Divides two numbers
 *
 * @param a {Number} - Numerator
 * @param b {Number} - Denominator
 *
 * @returns {Number} - Quotient of operands
 */
function divide(a, b) {
    if (!(isNumeric(a) && isNumeric(b))) {
        throw new NonNumericArgumentException(arguments.callee.name);
    }
    return (a / b);
}

/**
 * Calculates the remainder when dividing two numbers
 *
 * @param a {Number} - Numerator
 * @param b {Number} - Denominator
 *
 * @returns {Number} - Remainder after division of operands
 */
function mod(a, b) {
    if (!(isNumeric(a) && isNumeric(b))) {
        throw new NonNumericArgumentException(arguments.callee.name);
    }
    return (a % b);
}

/**
 * Determines whether a given value represents a number
 *
 * @param n {*} Value to check whether numeric
 *
 * @returns {boolean} true if n represents a number; false otherwise.
 */
function isNumeric(n) {
    return !isNaN(parseFloat(n));
}

/**
 * An Exception that occurs when one of the math functions receives a non-numeric input value
 *
 * @param where {String} the name of the function that is throwing the Exception
 *
 * @constructor
 */
function NonNumericArgumentException(where) {
    var messageTail = "method received a non-numeric argument; please ensure you are providing numeric values.";
    this.name = "NonNumericArgumentException";
    this.message = ["The", where, messageTail].join(" ");
}

