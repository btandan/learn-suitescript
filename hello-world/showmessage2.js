/**
 * @NApiVersion 2.0
 * @NScriptType ClientScript
 * @appliedtorecord employee
 */
define(["N/currentRecord"], function (cr) {
    function showMessage() {
        var email = cr.get().getValue({fieldId: "email"});
        var message = "From 2.0 your email is " + email;
        alert(message);
    }

    return {
        pageInit: showMessage
    };
});
