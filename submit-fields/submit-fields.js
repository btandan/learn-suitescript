/**
 * A SuiteScript 1.0 example of using nlapiSubmitField to update a single field on a related record
 */

// From a Sales Order, get the Customer ID
var customerId = nlapiGetFieldValue("entity");

// Set a comment on the Customer record
nlapiSubmitField("customer", customerId, "comments", "This is a comment added by inline editing with SuiteScript.");
