/**
 * A SuiteScript 1.0 example of using nlapiSubmitField to update multiple fields on a related record
 */

// From a Sales Order, get the Customer ID
var customerId = nlapiGetFieldValue("entity");

// Set a Comment and update the Budget Approved field on the Customer record
nlapiSubmitField("customer", customerId,
    ["comments", "isbudgetapproved"],
    ["The budget has been approved.", "T"]);
