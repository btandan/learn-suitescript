/**
 * A SuiteScript 2.0 example of using N/record#submitFields to update multiple fields on a related record
 */

require(["N/record", "N/currentRecord"], function (r, cr) {

    // From a Sales Order, get the Customer ID
    var customerId = cr.get().getValue({"fieldId": "entity"});

    // Set a Comment and check the Budget Approved box on the Customer record
    r.submitFields({
        "type": r.Type.CUSTOMER,
        "id": customerId,
        "values": {
            "comments": "The budget has been approved.",
            "isbudgetapproved": true
        }
    });
});
