This directory contains examples of inline editing using SuiteScript.

In the NetSuite UI, users are able to inline-edit records, which allows the user to very quickly modify and update
the data for a particular record without having to load the entire record on a page, edit the form, then save
the record.

There is a corresponding functionality provided in SuiteScript so that developers can use the same process in code;
this functionality is called `submitFields`. The `submitFields` functionality is provided by:

* the `nlapiSubmitField` global function in SuiteScript 1.0
* the `N/record#submitFields` method in SuiteScript 2.0

The `submitFields` functionality is a companion feature to the `lookupFields` functionality.

## Performance and Limitations

`submitFields` performs significantly faster and uses less governance than making the same changes by loading
and submitting the full record.

Multiple fields can be updated at once for the same cost as updating a single field. Updating more fields with
`submitFields` *does not* incur a higher governance cost.

However, you must be aware that only certain fields on each record type are inline-editable, and the performance
savings *only* applies to these inline-editable fields. If you use the `submitFields` function on any
non-inline-editable field, the field *will* be updated correctly, but behind the scenes, NetSuite will actually
load and submit the record, thus taking more time and using more governance. You can determine whether a field
is inline-editable by referring to the "nlapiSubmitField" column in the Records Browser.

`submitFields` functionality is also limited to the *body* fields of a record. If you need to modify sublist data,
you will need to load the record to make your changes, then submit the record.

## References:

* NetSuite Help: "Inline Editing and SuiteScript Overview"
* NetSuite Help: "Inline Editing Using nlapiSubmitField"
* NetSuite Help: "Consequences of Using nlapiSubmitField on Non Inline Editable Fields"
* NetSuite Help: "Field APIs"
* NetSuite Help: "record.submitFields(options)"