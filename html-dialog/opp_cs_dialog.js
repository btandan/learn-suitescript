define(["N/ui/dialog", "N/https"], function (dialog, https) {

    /**
     * Module Description...
     *
     * @exports XXX
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType ClientScript
     */
    var exports = {};


    /**
     * <code>fieldChanged</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.currentRecord
     *        {record} The current record the user is manipulating in the UI
     * @param context.sublistId
     *        {String} The internal ID of the sublist.
     * @param context.fieldId
     *        {String} The internal ID of the field that was changed.
     * @param [context.lineNum=undefined]
     *        {String} The index of the line if the field is in a sublist or
     *            matrix.
     * @param [context.columnNum=undefined]
     *        {String} The index of the column if the field is in a matrix.
     *
     * @return {void}
     *
     * @static
     * @function fieldChanged
     */
    function fieldChanged(context) {
        if (context.fieldId !== "entitystatus") {
            return;
        }

        var entityStatus = context.currentRecord.getValue({fieldId: "entitystatus"});

        if (entityStatus != "13") { // Closed Won
            return;
        }

        // show dialog
        dialog.create({
            title: "Example Dialog",
            message: getHtml()
        });
    }

    function getHtml() {
        return https.get({url: "/core/media/media.nl?id=6489&c=TSTDRV1555136&h=6b1e0e694d6726cd97d6"}).body;
    }

    exports.fieldChanged = fieldChanged;
    return exports;
});
