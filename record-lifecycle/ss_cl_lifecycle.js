define([], function () {

    /**
     * Module Description...
     *
     * @exports XXX
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType ClientScript
     */
    var exports = {};

    /**
     * <code>pageInit</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.mode
     *        {String} The access mode of the current record. Will be one of
     *            <ul>
     *            <li>copy</li>
     *            <li>create</li>
     *            <li>edit</li>
     *            </ul>
     *
     * @return {void}
     *
     * @static
     * @function pageInit
     */
    function pageInit(context) {
        console.info("CS pageInit triggered: " + context.mode);
    }

    /**
     * <code>validateField</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.currentRecord
     *        {record} The current record the user is manipulating in the UI
     * @param context.sublistId
     *        {String} The internal ID of the sublist.
     * @param context.fieldId
     *        {String} The internal ID of the field being validated.
     * @param [context.lineNum=undefined]
     *        {String} The index of the line if the field is in a sublist or
     *            matrix.
     * @param [context.columnNum=undefined]
     *        {String} The index of the column if the field is in a matrix.
     *
     * @return {Boolean} <code>true</code> if the field is valid.
     *         <code>false</code> to prevent the field value from changing.
     *
     * @static
     * @function validateField
     */
    function validateField(context) {
        console.info("CS validateField triggered: " + context.fieldId);
        return true;
    }

    /**
     * <code>fieldChanged</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.currentRecord
     *        {record} The current record the user is manipulating in the UI
     * @param context.sublistId
     *        {String} The internal ID of the sublist.
     * @param context.fieldId
     *        {String} The internal ID of the field that was changed.
     * @param [context.lineNum=undefined]
     *        {String} The index of the line if the field is in a sublist or
     *            matrix.
     * @param [context.columnNum=undefined]
     *        {String} The index of the column if the field is in a matrix.
     *
     * @return {void}
     *
     * @static
     * @function fieldChanged
     */
    function fieldChanged(context) {
        console.info("CS fieldChanged triggered: " + context.fieldId);
    }

    /**
     * <code>postSourcing</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.currentRecord
     *        {record} The current record the user is manipulating in the UI
     * @param context.sublistId
     *        {String} The internal ID of the sublist.
     * @param context.fieldId
     *        {String} The internal ID of the field that triggered
     *            <code>postSourcing</code>.
     *
     * @return {void}
     *
     * @static
     * @function postSourcing
     */
    function postSourcing(context) {
        console.info("CS postSourcing triggered: " + context.fieldId);
    }

    /**
     * <code>lineInit</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.currentRecord
     *        {record} The current record the user is manipulating in the UI
     * @param context.sublistId
     *        {String} The internal ID of the sublist.
     *
     * @return {void}
     *
     * @static
     * @function lineInit
     */
    function lineInit(context) {
        console.info("CS lineInit triggered: " + context.sublistId);
    }

    /**
     * <code>validateLine</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.currentRecord
     *        {record} The current record the user is manipulating in the UI
     * @param context.sublistId
     *        {String} The internal ID of the sublist.
     *
     * @return {Boolean} <code>true</code> if the line is valid;
     *         <code>false</code> to prevent the line submission.
     *
     * @static
     * @function validateLine
     */
    function validateLine(context) {
        console.info("CS validateLine triggered: " + context.sublistId);
        return true;
    }

    /**
     * <code>validateInsert</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.currentRecord
     *        {record} The current record the user is manipulating in the UI
     * @param context.sublistId
     *        {String} The internal ID of the sublist.
     *
     * @return {Boolean} <code>true</code> if the line can be inserted;
     *         <code>false</code> to prevent the line insertion.
     *
     * @static
     * @function validateInsert
     */
    function validateInsert(context) {
        console.info("CS validateInsert triggered: " + context.sublistId);
        return true;
    }

    /**
     * <code>validateDelete</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.currentRecord
     *        {record} The current record the user is manipulating in the UI
     * @param context.sublistId
     *        {String} The internal ID of the sublist.
     *
     * @return {Boolean} <code>true</code> if the line can be removed;
     *         <code>false</code> to prevent the line removal.
     *
     * @static
     * @function validateDelete
     */
    function validateDelete(context) {
        console.info("CS validateDelete triggered: " + context.sublistId);
        return true;
    }

    /**
     * <code>sublistChanged</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.currentRecord
     *        {record} The current record the user is manipulating in the UI
     * @param context.sublistId
     *        {String} The internal ID of the sublist.
     *
     * @return {void}
     *
     * @static
     * @function sublistChanged
     */
    function sublistChanged(context) {
        console.info("CS sublistChanged triggered: " + context.sublistId);
    }

    /**
     * <code>saveRecord</code> event handler
     *
     * @governance XXX
     *
     * @return {Boolean} <code>true</code> if the record is valid;
     *         <code>false</code> to stop form submission.
     *
     * @static
     * @function saveRecord
     */
    function saveRecord() {
        console.info("CS saveRecord triggered");
        return true;
    }

    exports.pageInit = pageInit;
    exports.validateField = validateField;
    exports.fieldChanged = fieldChanged;
    exports.postSourcing = postSourcing;
    exports.lineInit = lineInit;
    exports.validateLine = validateLine;
    exports.validateInsert = validateInsert;
    exports.validateDelete = validateDelete;
    exports.sublistChanged = sublistChanged;
    exports.saveRecord = saveRecord;
    return exports;
});
