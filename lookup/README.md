This directory contains examples of Lookup functionality in SuiteScript.

When processing a given record, you will oft need to retrieve data from one of its related records. For example,
when working with a given Sales Order, you may need to retrieve data from the related Sales Rep. In SuiteScript
terminology, this is called a **lookup**.

Lookup functionality is provided by:

* the `nlapiLookupField` global function in SuiteScript 1.0
* `N/search#lookupFields` in SuiteScript 2.0

## Performance

A Lookup is just shorthand for performing a search that filters on the internal ID of a single record for the
result. Under the hood, lookups are actually performing a search, so the performance will be similar to that of
a search that returns a single record.

This also means that a lookup will perform faster than loading the record to retrieve the same information.

## Limitations

Lookups can only be used to retrieve body field data. You cannot retrieve data from the sublists of a related
record using a lookup. If you need sublist data, you will either need to perform a search or load the related
record.