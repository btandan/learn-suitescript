/**
 * An example of nlapiLookupField to retrieve a single field from a related record
 */

// Get the Sales Rep record ID
var repId = nlapiGetFieldValue("salesrep");

// Get the name of the Sales Rep
var repName = nlapiGetFieldText("salesrep");

// Retrieve the email address from the associated Sales Rep
var repEmail = nlapiLookupField("employee", repId, "email");

console.log(repEmail);
console.log(repName + "'s email address is " + repEmail);