define(["N/search", "N/file"], function (s, f) {

    /**
     * An example of reading in CSS and HTML files and adding dynamic data to them
     *
     * @exports ss/example-html/sl
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType Suitelet
     */
    var exports = {};

    /**
     * <code>onRequest</code> event handler
     *
     * @governance 30
     *
     * @param context
     *        {Object}
     * @param context.request
     *        {ServerRequest} The incoming request object
     * @param context.response
     *        {ServerResponse} The outgoing response object
     *
     * @return {void}
     *
     * @static
     * @function onRequest
     */
    function onRequest(context) {
        context.response.write({output: renderPage()});
    }

    function renderPage() {
        var css = loadStyles();
        var html = loadHtml();
        var customerData = findCustomers();

        // Replace the CSS placeholder
        html = html.replace("CSS_HERE", css);

        // Replace the HTML placeholder with translated search results
        html = html.replace("RESULTS_HERE", resultsToHtml(customerData));

        return html;
    }

    // Reads the CSS file from the File Cabinet
    function loadStyles() {
        return f.load({id:"SuiteScripts/html-placeholder/example.css"}).getContents();
    }

    // Reads the HTML file from the File Cabinet
    function loadHtml() {
        return f.load({id:"SuiteScripts/html-placeholder/example.html"}).getContents();
    }

    // Performs a search
    function findCustomers() {
        return s.create({
            type: s.Type.CUSTOMER,
            filters: [
                ["companyname", s.Operator.STARTSWITH, "B"], "and",
                ["isinactive", s.Operator.IS, "F"]
            ],
            columns: ["companyname", "state"]
        }).run().getRange({start: 0, end: 10});
    }

    // Translates a single result to an HTML table row
    function resultToHtml(result) {
        return "<tr><td>" +
            result.getValue({name:"companyname"}) +
            "</td><td>" +
            result.getValue({name:"state"}) +
            "</td></tr>";
    }

    // Translates all results in the list to an HTML table row,
    // then joins them all into a single String
    function resultsToHtml(results) {
        return results.map(resultToHtml).join("");
    }

    exports.onRequest = onRequest;
    return exports;
});
