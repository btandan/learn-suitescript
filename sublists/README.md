This directory contains examples of working with sublists in SuiteScript.

## Introduction

NetSuite Records are divided into Body fields and Sublists. There are four types of sublists:

* Static
* Editor
* Inline Editor
* List

We are able to add, insert, edit, and remove line items using Sublist APIs.

For a reference on exactly which sublists support SuiteScript, see the NetSuite Help page titled "Scriptable
Sublists".

## Sublist Indices

Each line item in a sublist has an index that we can use to reference it.

In SuiteScript 1.0, these indices are `1`-based, so the first line item has index `1`, the second has index `2`,
and so on.

In SuiteScript 2.0, these indices are `0`-based, so the first line item has index `0`, the second has index `1`,
and so on. This of course more closely matches Array indexing in most languages, including JavaScript.

## Standard vs Dynamic Mode

The API we use for interacting with a sublist depends on whether we are working with the record in
[Standard or Dynamic mode](/training/curriculum/records/record-modes).

The Standard-mode APIs simply let us provide the index of the line we want to work with as a parameter to the
appropriate function.

The Dynamic-mode APIs follow a pattern:

1. Select the line we want to work with
1. Modify the selected line as desired
1. Commit the changes to the line

In Dynamic Mode, if we do not commit the changes to *each* line we modify, then those changes will not be
reflected when the record is saved.

## Limitations

In order to work with sublist data via SuiteScript, we must have a reference in memory to the record. This means
the record either needs to be retrieved from the script context, or we need to load the record from the database. 

We *cannot* work with sublists via either [lookup](/training/curriculum/search/lookup) or
[submitFields](/training/curriculum/inline-edit/submit-fields) functionality.

*Static* sublists do not support SuiteScript at all.

## References:

* NetSuite Help: "What is a Sublist?"
* NetSuite Help: "Sublist Types"
* NetSuite Help: "Scriptable Sublists"
* NetSuite Help: "Working with Sublist Line Items"
* NetSuite Help: "Sublist APIs"
* NetSuite Help: "Working with Records in Dynamic Mode"