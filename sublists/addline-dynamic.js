// Adding a line item to the end of a sublist in Dynamic Mode...

// ... if the record is in context:
nlapiSelectNewLineItem("item");
nlapiSetCurrentLineItemValue("item", "item", 456);
nlapiSetCurrentLineItemValue("item", "quantity", 10);
nlapiCommitLineItem("item");

// ... or if we have a reference to the record (rec):
rec.selectNewLineItem("item");
rec.setCurrentLineItemValue("item", "item", 456);
rec.setCurrentLineItemValue("item", "quantity", 10);
rec.commitLineItem("item");